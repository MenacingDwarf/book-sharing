from django.db import models
from django.contrib.auth.models import User


class Book(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(max_length=50)
    lat = models.CharField(max_length=20)
    lon = models.CharField(max_length=20)
    owner = models.ForeignKey(User, models.CASCADE, related_name="shared_books")
    image = models.ImageField(null=True, default=None, blank=True)

    def __str__(self):
        return self.title
