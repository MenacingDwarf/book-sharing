from django.urls import path
from .views import *

urlpatterns = [
    path('hello/', hello),
    path('books/', books),
    path('log_in/', log),
    path('log_out/', log_out),
    path('register/', sign),
    path('user_info/', user_info)
]
