from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login
from django.db.models import Q
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

from .models import *


def hello(request):
    return JsonResponse({"data": "hellO!"})


@csrf_exempt
def books(request):
    if request.method == 'GET':
        all_books = Book.objects.filter(~Q(owner=request.user.id))
        return JsonResponse({"status": 200, "message": "success", "books": serializers.serialize("json", all_books, ensure_ascii=False)})
    else:
        post_data = request.POST
        user_obj = User.objects.get(id=request.user.id)
        new_book = Book(title=post_data["title"], description=post_data["description"],
                        lat=post_data["lat"], lon=post_data["lon"], owner=user_obj)
        new_book.save()
        return redirect("/")


@csrf_exempt
def user_info(request):
    user_obj = User.objects.get(id=request.user.id)
    books_shared = Book.objects.filter(owner=user_obj)
    return JsonResponse({"status": 200, "message": "success", "data": {
        "username": request.user.username,
        "email": request.user.email,
        "shared_books": serializers.serialize("json", books_shared, ensure_ascii=False)
    }})


@csrf_exempt
def log(request):
    if request.method == 'POST':
        user = authenticate(username=request.POST['user'], password=request.POST['pass'])
        if user and user.is_active is True:
            login(request, user)
            return JsonResponse({"status": 200, "message": "success", "data": {
                "username": user.username,
                "email": user.email,
            }})
        else:
            return JsonResponse({"status": 400, 'message': 'Неверный логин или пароль', "data": {}})


@csrf_exempt
def sign(request):
    if request.method == 'POST':
        try:
            user = User.objects.create_user(username=request.POST['user'], password=request.POST['pass'])
            user.save()
            authenticate(username=request.POST['user'], password=request.POST['pass'])
        except:
            return JsonResponse({"status": 400, 'message': 'Пользователь уже зарегистрирован', "data": {}})

        return JsonResponse({"status": 200, "message": "success", "data": {
            "username": user.username,
            "email": user.email,
        }})


@csrf_exempt
def log_out(request):
    logout(request)
    return redirect("/")
