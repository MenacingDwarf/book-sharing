import React, {Component} from 'react';
import {Map, Placemark, YMaps} from "react-yandex-maps";

class BooksMap extends Component {
    render() {
        let placemarks = this.props.books ? this.props.books.map(book => {
            return <Placemark key={book.pk} geometry={[book.fields.lat, book.fields.lon]}
                              defaultProperties={{
                                  balloonContentBody: book.fields.description,
                                  balloonContentHeader: book.fields.title,
                                  iconLayout: 'default#imageWithContent',
                                  iconImageHref: book.fields.image,
                              }} modules={['geoObject.addon.balloon']}
            />
        }) : null;
        let map = this.props.books ? <YMaps>
            <div>
                <Map defaultState={{center: [59.939037, 30.315777], zoom: 11}} style={{width: "100%", height: "600px"}}
                     modules={["templateLayoutFactory", "layout.ImageWithContent"]}>
                    {placemarks}
                </Map>
            </div>
        </YMaps> : <YMaps>
            <div>
                <Map defaultState={{center: [59.939037, 30.315777], zoom: 11}}/>
            </div>
        </YMaps>;
        return (
            <div>
                {map}
            </div>
        );
    }
}

export default BooksMap;