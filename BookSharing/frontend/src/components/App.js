import React, {Component} from 'react'
import {YMaps, Map} from 'react-yandex-maps';
import {BrowserRouter, Route} from 'react-router-dom';
import MainPage from "./MainPage";
import LoginPage from "./LoginPage";
import Cookies from "js-cookie";
import RegistrationPage from "./RegistrationPage";
import Header from "./Header";

class App extends Component {
    state = {
        is_auth: null,
        user_info: null,
    };

    componentDidMount() {
        let username = Cookies.get('username');
        if (username) {
            this.setState({is_auth: true})
        } else {
            this.setState({is_auth: false})
        }
    }

    log_in = (user_info) => {
        Cookies.set("username", user_info.username);
        this.setState({
            is_auth: true,
            user_info: user_info,
        })
    };

    log_out = () => {
        Cookies.remove("username");
        this.setState({
            is_auth: false,
            user_info: null
        });
        fetch('/api/log_ot');
    };

    render() {
        let main = this.state.is_auth === null ? <div>Loading...</div> : (
            this.state.is_auth ? <MainPage/> : <LoginPage log_in={this.log_in}/>
        );
        let content = <BrowserRouter>
            <Header is_auth={this.state.is_auth} log_out = {this.log_out}/>
            <div className="container">
                <Route exact path="/" render={(routeProps) => (
                    this.state.is_auth === null ? <div>Loading...</div> : (
                        this.state.is_auth ? <MainPage/> : <LoginPage log_in={this.log_in}/>
                    ))}/>
                <Route path={"/registration"} render={(routeProps) => (
                    <RegistrationPage {...routeProps} log_in={this.log_in}/>
                )}/>
            </div>
        </BrowserRouter>;
        return <div>
            {content}
        </div>
    }
}

export default App;