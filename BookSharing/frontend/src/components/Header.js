import React, {Component} from 'react';
import {Link} from 'react-router-dom'

class Header extends Component {
    logoutHandler= (e) => {
        this.props.log_out();
    };

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-info p-2 mb-2">
                <Link className="navbar-brand pl-3" to="/">
                    <h4>Book Sharing</h4>
                </Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav mr-auto">
                    </ul>
                    {
                        this.props.is_auth ?
                        <form className="form-inline my-2 my-lg-0">
                            <a className="navbar-brand" onClick={this.logoutHandler}>Выйти</a>
                        </form> : null
                    }
                </div>
            </nav>
        );
    }
}

export default Header;