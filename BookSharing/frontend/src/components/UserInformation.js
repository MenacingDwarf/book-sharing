import React, {Component} from 'react';

class UserInformation extends Component {
    state = {
        username: null,
        email: null,
        shared_books: null,
        adding_open: false
    };

    componentDidMount() {
        fetch("/api/user_info")
            .then(response => response.json())
            .then(result => {
                this.setState({
                    username: result.data.username,
                    email: result.data.email,
                    shared_books: JSON.parse(result.data.shared_books)
                });
            })
    };

    render() {
        let state = this.state;
        console.log(state);
        let user_info_comp = state.username ?
            <div className={"card mb-2"}>
                <div className="card-header">
                    <h4 className="card-title">User information</h4>
                </div>
                <div className="card-body">
                    <p><b>Никнейм</b>: {state.username}</p>
                    <p><b>Email</b>: {state.email}</p>
                    <p><b>Количество отданных книг</b>: {this.state.shared_books.length}</p>
                </div>
            </div>
            : <p>Загрузка...</p>;
        let adding_book_form = <div>
             <button type="button" className="btn btn-outline-info mt-1 mb-3" data-toggle="collapse"
                            data-target="#adding_book_form"
                            onClick={(e) => this.setState({adding_open: !this.state.adding_open})}>
                        {this.state.adding_open ? "Скрыть" : "Добавить книгу"}
                    </button>
                    <div className="collapse" id="adding_book_form">
                        <form action="/api/books/" method="post">
                            <div className="form-group">
                                <label htmlFor="bookTitle">Название</label>
                                <input type="text" className="form-control" id="bookTitle" name="title"
                                       placeholder="Евгений Онегин" required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="bookAuthor">Автор</label>
                                <input type="text" className="form-control" id="bookAuthor" name="author"
                                       placeholder="Пушкин А.С." required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="bookDescription">Описание</label>
                                <input type="text" className="form-control" id="bookDescription" name="description"
                                       placeholder="Я вас любил, чего же боле..." required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="bookLat">Координата lat</label>
                                <input type="text" className="form-control" id="bookLat" name="lat"
                                       placeholder="59.939037" required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="bookLon">Координата lon</label>
                                <input type="text" className="form-control" id="bookLon" name="lon"
                                       placeholder="30.315777" required/>
                            </div>
                            <center><button type="submit" className="btn btn-dark mb-2">Добавить книгу</button></center>
                        </form>
                    </div>
        </div>
        return (
            <div>
                {user_info_comp}
                {adding_book_form}
            </div>
        );
    }
}

export default UserInformation;