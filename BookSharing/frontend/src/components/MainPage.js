import React, {Component} from 'react';
import {Map, YMaps, Placemark} from "react-yandex-maps";
import BooksMap from "./BooksMap";
import UserInformation from "./UserInformation";

class MainPage extends Component {
    state = {
        books: null
    };

    componentDidMount() {
        fetch("/api/books")
            .then(response => response.json())
            .then(result => {
                this.setState({books: JSON.parse(result.books)});
            })
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-6">
                        <UserInformation/>
                    </div>
                    <div className="col-6">
                        {this.state.books ?
                            <BooksMap books={this.state.books}/>
                            :
                            <p>Загрузка...</p>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default MainPage;