import React, {Component} from 'react';
import Cookies from "js-cookie";
import {Link} from "react-router-dom";

class RegistrationPage extends Component {
    state = {
        message: null
    };

    sendToServer = (form) => {
        let comp = this;
        let csrf = Cookies.get('csrftoken');
        let xhr = new XMLHttpRequest();
        let formData = new FormData(form);
        xhr.open("POST", '/api/register/', true);
        //xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.setRequestHeader('X-CSRFToken', csrf);
        xhr.onreadystatechange = function () {
            if (this.readyState !== 4) return;
            let answer = JSON.parse(this.responseText);
            if (answer.status === 200) {
                comp.props.log_in(answer.data);
                comp.props.history.push('/');
            } else comp.setState({
                message: answer.message,
            })
        };

        xhr.send(formData);
    };

    handlerSubmit = (e) => {
        e.preventDefault();
        this.setState({
            message: "Проверка логина и пароля..."
        });
        this.sendToServer(e.target);
    };

    render() {
        let message = this.state.message ? <p className={"alert alert-danger"}>{this.state.message}</p> : null;
        return (<div>
                <center><h1>Зарегистрируйтесь</h1></center>
                <div className="card w-50 m-auto">
                    <div className="card-body">
                        <form action="/register/" method="post" onSubmit={this.handlerSubmit}>
                            {message}
                            <div className="form-group">
                                <h4>Заполните все поля необходимые для регистрации</h4>
                                <label htmlFor="exampleInputEmail1">Адрес электронной почты</label>
                                <input type="text" className="form-control" id="exampleInputEmail1" name="user"
                                       aria-describedby="emailHelp"
                                       placeholder="example@mail.ru" required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Пароль</label>
                                <input type="password" className="form-control" id="exampleInputPassword1" name="pass"
                                       placeholder="*********"
                                       required/>
                            </div>
                            <center><button type="submit" className="btn btn-dark">Зарегистрироваться</button></center>
                        </form>
                    </div>
                    <div className="card-footer">
                        <div className="alert alert-info">Already have account? <Link to={"/"}>Sign in</Link>!
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}

export default RegistrationPage;